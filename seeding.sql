use Gimnasio_v3
go

insert into Administracion.Departamento values ('Managua'), ('Masaya'), ('Le�n'), ('Granada'), 
('Carazo'), ('Estel�'), ('Rivas'), ('Chinandega'), ('Madriz'), ('Matagalpa'), 
('Nueva Segovia'), ('Boaco'), ('R�o San Juan'), ('Costa Caribe Sur'), 
('Jinotega'), ('Costa Caribe Norte'), ('Chontales')
go

insert into Administracion.Municipio values ('Managua', 1), ('Ciudad Sandino', 1), ('El Crucero', 1), ('Mateare', 1),
('San Francisco Libre', 1), ('San Rafael del Sur', 1), ('Ticuantepe', 1), ('Tipitapa', 1),
('Villa El Carmen', 1), ('Catarina', 2), ('La Concepci�n', 2), ('Masatepe', 2), ('Masaya', 2),
('Nandasmo', 2), ('Nindir�', 2), ('Niquinohomo', 2), ('San Juan de Oriente', 2),
('Tisma', 2), ('Achuapa', 3), ('El Jicaral', 3), ('El Sauce', 3), ('La Paz Centro', 3),
('Larreynaga', 3), ('Le�n', 3), ('Nagarote', 3), ('Quezalguaque', 3), ('Santa Rosa del Pe��n', 3),
('Telica', 3), ('Granada', 4), ('Diri�', 4), ('Diriomo', 4), ('Nandaime', 4), ('Jinotepe', 5),
('Diriamba', 5), ('Dolores', 5), ('El Rosario', 5), ('La Conquista', 5), ('La Paz', 5),
('San Marco', 5), ('Santa Teresa', 5), ('Estel�', 6), ('Condega', 6), ('La Trinidad', 6),
('Pueblo Nuevo', 6), ('San Juan de Limay', 6), ('San Nicol�s', 6), ('Rivas', 7),
('Altagracia', 7), ('Bel�n', 7), ('Buenos Aires', 7), ('C�rdenas', 7), ('Moyogalpa', 7),
('Potos�', 7), ('San Jorge', 7), ('San Juan del Sur', 7), ('Tola', 7), ('Chinandega', 8),
('Chichigalpa', 8), ('Corinto', 8), ('El Realejo', 8), ('El Viejo', 8), ('Posoltega', 8),
('Puerto Moraz�n', 8), ('San Francisco del Norte', 8), ('San Juan de Cinco Pinos', 8),
('San Pedro del Norte', 8), ('Santo Tom�s del Norte', 8), ('Somotillo', 8), ('Villanueva', 8),
('Somoto', 9), ('Las Sabanas', 9), ('Palacag�ina', 9), ('San Jos� de Cusmapa', 9),
('San Juan de R�o Coco', 9), ('San Lucas', 9), ('Telpaneca', 9), ('Totogalpa', 9),
('Yalag�ina', 9), ('Matagalpa', 10), ('Ciudad Dar�o', 10), ('Esquipulas', 10), ('Matigu�s', 10),
('Muy Muy', 10), ('Rancho Grande', 10), ('R�o Blanco', 10), ('San Dionisio', 10),
('San Isidro', 10), ('San Ram�n', 10), ('S�baco', 10), ('Terrabona', 10), ('Tuma � La Dalia', 10),
('Ocotal', 11), ('Ciudad Antigua', 11), ('Dipilto', 11), ('El J�caro', 11), ('Jalapa', 11),
('Macuelizo', 11), ('Mozonte', 11), ('Murra', 11), ('Quilal�', 11), ('San Fernando', 11), 
('Santa Mar�a', 11), ('Wiwil�', 11), ('Boaco', 12), ('Camoapa', 12), ('San Jos� de los Remates', 12),
('San Lorenzo', 12), ('Santa Luc�a', 12), ('Teustepe', 12), ('San Carlos', 13), ('El Almendro', 13),
('El Castillo', 13), ('Morrito', 13), ('San Juan del Norte', 13), ('San Miguelito', 13),
('Bluefields', 14), ('Corn Island', 14), ('Desembocadura de R�o Grande', 14), ('El Ayote', 14),
('El Rama', 14), ('El Tortuguero', 14), ('Kukra Hill', 14), ('La Cruz de R�o Grande', 14),
('Laguna de Perlas', 14), ('Muelle de los Bueyes', 14), ('Nueva Guinea', 14), ('Paiwas', 14),
('Jinotega', 15), ('El Cu�', 15), ('La Concordia', 15), ('San Jos� de Bocay', 15),
('San Rafael del Norte', 15), ('San Sebasti�n de Yal�', 15), ('Santa Mar�a de Pantasma', 15),
('Wiwil�', 15), ('Puerto Cabezas', 16), ('Bonanza', 16), ('Mulukuk�', 16), ('Prinzapolka', 16),
('Rosita', 16), ('Siuna', 16), ('Waslala', 16), ('Wasp�n', 16), ('Juigalpa', 17),
('Acoyapa', 17), ('Comalapa', 17), ('El Coral', 17), ('La Libertad', 17), ('San Francisco de Cuapa', 17),
('San Pedro de L�vago', 17), ('Santo Domingo', 17), ('Santo Tom�s', 17), ('Villa Sandino', 17)
go

----insert into Administracion.MotivoDevolucion values
----('ERROR DE DESPACHO', 'EL PRODUCTO O LA CANTIDAD DESPACHADOS NO CORRESPONDEN A LO FACTURADO'),
----('ERROR EN FACTURACION', 'EL PRODUCTO O LA CANTIDAD FACTURADAS, NO CORRESPONDEN A LO SOLICITADO'),
----('ERROR DE PEDIDO', 'ERROR DEL CLIENTE AL ELABORAR EL PEDIDO'),
----('APLICACI�N', 'EL PRODUCTO NO APLICA O ESTA MAL ROTULADO'),
----('GARANTIA', 'EL PRODUCTO TIENE MAL FUNCIONAMIENTO O ESTA DA�ADO')
----go

--CCN raptor

--coca 
--pawerade
--alpina

--gatorade
--amp 365
--adrenaline

--exec Compras.SPInsertarProveedor 'Compa�ia Cervecera de Nicaragua', 1, 'Compa�ia Cervecera de Nicaragua, Managua 11021', '22557700', '', 'https://www.ccn.com.ni/'
--exec Compras.SPInsertarProveedor 'Coca Cola FEMSA Managua', 1, 'Coca-Cola, Managua 11009', '22557440', '', 'https://www.coca-colafemsa.com/'
--exec Compras.SPInsertarProveedor 'Pepsi Planta Managua (Embotelladora Nacional)', 1, 'Managua', '75058259', '', ''

--exec Administracion.SPInsertarPlan 'D�a', '', 1, 2, 25
--exec Administracion.SPInsertarPlan 'Semanal', '', 7, 2, 120
--exec Administracion.SPInsertarPlan 'Quincenal', '', 15, 2, 300
--exec Administracion.SPInsertarPlan 'Mensual', '', 30, 2, 500
--exec Administracion.SPInsertarPlan 'Sabatino 1 d�a', '', 1, 4, 40

