use Gimnasio_v3
go

create procedure sp_listar_departamentos as
	select * from Administracion.Departamento
go

create procedure sp_listar_municipios_por_departamento(@id_departamento int) as
	select * from Administracion.Municipio where id_departamento = @id_departamento
go

create procedure sp_listar_proveedores as
	select * from Compras.Proveedor where es_activo = 1
go
