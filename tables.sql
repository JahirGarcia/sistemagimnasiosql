create database Gimnasio_v3
go

use Gimnasio_v3
go

create schema Administracion
go

create schema Compras
go

create schema Ventas
go

create table Administracion.Departamento(
id_departamento int identity(1, 1) primary key not null,
nombre nvarchar(18) not null
)
go

create table Administracion.Municipio(
id_municipio int identity(1, 1) primary key not null,
nombre nvarchar(27) not null,
id_departamento int not null foreign key references Administracion.Departamento(id_departamento)
)
go

create table Compras.Proveedor(
id_proveedor uniqueidentifier primary key not null,
nombre nvarchar(32) not null,
id_municipio int not null foreign key references Administracion.Municipio(id_municipio),
direccion nvarchar(64) not null,
telefono char(8) check(telefono like '[2|5|7|8][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
correo_electronico nvarchar(64) check(correo_electronico like '%_@_%_.__%'),
url_sitio_web nvarchar(64) check(url_sitio_web like 'http%://%_.__%'),
es_activo bit not null default 1
)
go
